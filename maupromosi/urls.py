"""maupromosi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

"""This urls is temporary, further action is needed"""
urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^', include('homepage.urls')),
    re_path(r'catalog/', include('catalog.urls')),
    re_path(r'post_details/', include('postdetails.urls')),
    re_path(r'user_profile/', include('userprofile.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
