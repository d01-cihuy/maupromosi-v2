from django import forms
from .models import Saran

class Input_saran(forms.ModelForm):
    
    class Meta:
        model = Saran
        fields = ['saran']
        widgets={
            'saran':forms.TextInput(attrs={
                'id':'saran',
                'required':True,
                'placeholder':'Saran...'
            })
        }