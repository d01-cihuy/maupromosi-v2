from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from django.http import HttpRequest

from .models import Visitor,Saran
from userprofile.models import User
from postdetails.models import Post, PostLocation, PostCategory
from .forms import Input_saran
from .views import saran

class Testing_catalog(TestCase):

    def test_url_catalog(self):
        response = Client().get('/catalog/')
        self.assertEqual(response.status_code, 200)

    def test_using_catalog_template(self):
        response = Client().get('/catalog/')
        self.assertTemplateUsed(response, 'catalog/katalog.html')
    
    def test_model_visitor(self):
        new_visitor = Visitor.objects.create(visit=1)
        jumlah_visitor= Visitor.objects.all().count()
        self.assertEqual(jumlah_visitor,1)

    def test_model_saran(self):
        new_saran = Saran.objects.create(saran="bagus")
        jumlah_saran= Saran.objects.all().count()
        self.assertEqual(jumlah_saran,1)
    
    def test_filter_lokasi(self):
        lokasi = PostLocation.objects.create(location="Jakarta")
        response = Client().get('/catalog/?lokasi=Jakarta')
        self.assertTrue(response)
    
    def test_filter_kategori(self):
        lokasi = PostCategory.objects.create(category="Makanan")
        response = Client().get('/catalog/?kategori=Makanan')
        self.assertTrue(response)
    
    def test_form_saran(self):
        form = Input_saran(data={'saran': 'bagus'})
        self.assertTrue(form.is_valid())
    
    #def test_view_form_saran(self):
        #saran = "bagus"
        #response = Client().post('/catalog/',{"saran": saran})
        #self.assertEqual(response.status_code,200)
        #html_response = response.content.decode('utf8')
        #self.assertIn(saran,html_response)
    
    def test_view_saran(self):
        Saran.objects.create(saran="bagus")
        request = HttpRequest()
        response = saran(request)
        html_response = response.content.decode('utf8')
        self.assertIn('bagus', html_response)
    
    def test_filter_nama(self):
        user = User.objects.create(
            username = "risw",
    email = "apa@gmail.com",
    name = "ris",
    instagram = "https://instagram.com/helo"
        )

        Post.objects.create(
            user=user,
            name="kopi",
            price="20000",
            description = "enak"
        )
        response = Client().get('/catalog/?cari_produk=kopi')
        self.assertTrue(response)
        response = Client().get('/catalog/?cari_produk=enak')
        self.assertTrue(response)
    
    def test_check(self):
        user = User.objects.create(
            username = "risw",
    email = "apa@gmail.com",
    name = "ris",
    instagram = "https://instagram.com/helo"
        )

        Post.objects.create(
            user=user,
            name="kopi",
            price="20000",
            description = "enak"
        )
        response = Client().post('/catalog/check/enak')
        self.assertEqual(response.status_code, 200)

    def test_location(self):
        user = User.objects.create(
            username = "risw",
    email = "apa@gmail.com",
    name = "ris",
    instagram = "https://instagram.com/helo"
        )

        post = Post(
            user=user,
            name="kopi",
            price="20000",
            description = "enak",
        )
        post.save()
        location = post.location.create(location='Jakarta')
        location.save()
        response = Client().post('/catalog/location/Jakarta')
        self.assertEqual(response.status_code, 200)

    def test_category(self):
        user = User.objects.create(
            username = "risw",
    email = "apa@gmail.com",
    name = "ris",
    instagram = "https://instagram.com/helo"
        )

        post = Post(
            user=user,
            name="kopi",
            price="20000",
            description = "enak",
        )
        post.save()
        category = post.category.create(category='Minuman')
        category.save()
        response = Client().post('/catalog/category/Minuman')
        self.assertEqual(response.status_code, 200)

    def test_all_show(self):
        response = Client().post('/catalog/all')
        self.assertEqual(response.status_code, 200)
    
