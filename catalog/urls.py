from django.urls import re_path,path
from .views import *

#Website urls
app_name = "catalog"

urlpatterns = [
    path('',katalog, name="katalog"),
    path('saran_katalog',saran, name="saran"),
    path('check/<cari>',check),
    path('location/<cari>',location),
    path('category/<cari>',category),
    path('all',all_show),
    path('collect_saran',collect_saran),
    path('api',api)
]