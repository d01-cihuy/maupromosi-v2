from django.shortcuts import render
from postdetails.models import Post, PostLocation, PostCategory, PostReview
from .models import Visitor,Saran
from .forms import Input_saran
from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse,JsonResponse
import json

# Create your views here.

def filter(request):
    db = Post.objects.all()
    nama_produk = request.GET.get("cari_produk")
    lokasi = request.GET.get("lokasi")
    kategori = request.GET.get("kategori")

    if is_valid_queryparam(nama_produk):
        if(db.filter(description__icontains = nama_produk)!= None ):
            db = db.filter(description__icontains = nama_produk)
                
    if is_valid_queryparam(lokasi):
        db = db.filter(location__location = lokasi)
    if is_valid_queryparam(kategori):
        db = db.filter(category__category = kategori)

    return db

def katalog(request):
    filtering = filter(request)
    visit = Visitor.objects.all()
    lokasi = PostLocation.objects.all()
    kategori = PostCategory.objects.all()
    count = request.session.get('count',0)
    newcount = count+1
    request.session['count'] = newcount
    visit.visit = newcount
    
    form = Input_saran()
    isi=collect_saran(request)

    context ={
        'post' : filtering,
        'visit': visit.visit,
        'lokasi' : lokasi,
        'kategori' : kategori,
        'form' : form
    }
    return render(request,'catalog/katalog.html', context)

def is_valid_queryparam(param):
    return param != '' and param is not None

def collect_saran(request):
    if request.method == 'POST':
        saran_text = request.POST.get('saran')
        response_data={}
        
        
        response_data['saran'] = saran_text

        Saran.objects.create(
            saran = saran_text
        )
        return JsonResponse(response_data)

def saran(request):
    saran = Saran.objects.all()
    content = {"saran" : saran }
    response_data = {}
    
    return render(request,'catalog/saran_katalog.html',content)

def check(request,cari):
    posts = Post.objects.filter(description__icontains=cari)
    
    return api(posts)

def location(request,cari):
    posts = Post.objects.filter(location__location=cari)
    
    return api(posts)

def category(request,cari):
    posts = Post.objects.filter(category__category=cari)
    return api(posts)

def all_show(request):
    posts = Post.objects.all()
    return api(posts)

def api(request):
    posts = request
    response = []
    for post in posts:
        if not post.location.all() or not post.category.all():
            location = ''
            category = ''
        else:
            location = ''
            category = ''
            for lokasi in post.location.all():
                if(location!=''):
                   location +=',' 
                location += lokasi.location
            for kategori in post.category.all():
                if(category!=''):
                   category +=',' 
                category += kategori.category                

        if(not post.image):
            image = ''
        else:
            image = post.image.url
        tmp = {
            'name': post.name,
            'image':image,
            'location':location,
            'category' : category,
            'description' : post.description,
            'pk':post.pk
        }
        response.append(tmp)
        
    reviews = JsonResponse({'fields':response})
    
    return reviews