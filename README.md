# MauPromosi

Web untuk promosi jualan yang ada di Instagram

---

## Project Status
* Pipeline status (Based on Master) : [![pipeline status](https://gitlab.com/d01-cihuy/maupromosi-v2/badges/master/pipeline.svg)](https://gitlab.com/d01-cihuy/maupromosi-v2/-/commits/master)
* Code coverage (Based on Staging)  : [![coverage report](https://gitlab.com/d01-cihuy/maupromosi-v2/badges/staging/coverage.svg)](https://gitlab.com/d01-cihuy/maupromosi-v2/-/commits/staging)

## Link Website
```https://maupromosi-v2.herokuapp.com```

## Creators
* Mohammad Riswanda Alifarahman ([@risw_24](https://gitlab.com/risw_24))
* Naufal Pramudya Yusuf ([@naufalpramudya](https://gitlab.com/naufalpramudya))
* Muhamad Nicky Salim ([@muhamadnickysalim](https://gitlab.com/muhamadnickysalim))
* Prajna ([@prajnapras19](https://gitlab.com/prajnapras19))

## Deskripsi
Banyak orang yang beralih profesi menjadi penjual produk (semisal makanan) selama pandemi Covid-19. Produk-produk tersebut ditawarkan melalui Instagram. Akan tetapi, sistem Instagram yang berbasis _follower_ membuat promosi tidak selalu berjalan mulus. Web ini berguna untuk membantu penjual mempromosikan produk yang dijualnya.

## Fitur
> To be added

## Notes
> To be added
