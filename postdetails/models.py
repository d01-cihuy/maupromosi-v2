from django.db import models
from maupromosi.settings import AUTH_USER_MODEL as User
from gdstorage.storage import GoogleDriveStorage

# Define Google Drive Storage
gd_storage = GoogleDriveStorage()

# Create your models here.
class PostCategory(models.Model):
    category = models.CharField(max_length=200)
    def __str__(self):
        return self.category

class PostLocation(models.Model):
    location = models.CharField(max_length=200)
    def __str__(self):
        return self.location
    
class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='maupromosi_v1/', storage=gd_storage, blank=True)
    price = models.PositiveIntegerField()
    location = models.ManyToManyField(PostLocation, blank=True)
    category = models.ManyToManyField(PostCategory, blank=True)
    description = models.CharField(max_length=2200)
    instagram_link = models.URLField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    def __str__(self):
        return self.name + " - " + str(self.user)

class PostReview(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    RATING_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    rating = models.IntegerField(choices=RATING_CHOICES)
    review = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    def __str__(self):
        return 'Review from %s for %s' % (str(self.user), str(self.post))
