from django.test import TestCase, Client
from django.conf import settings
from django.contrib.auth import login
from django.http import HttpRequest
from .models import *
from userprofile.models import *
from importlib import import_module

class TestClient(Client):
    # https://jameswestby.net/tech/17-directly-logging-in-a-user-in-django-tests.html
    def login_user(self, user):
        """
        Login as specified user, does not depend on auth backend (hopefully)

        This is based on Client.login() with a small hack that does not
        require the call to authenticate()
        """
        if not 'django.contrib.sessions' in settings.INSTALLED_APPS:
            raise AssertionError("Unable to login without django.contrib.sessions in INSTALLED_APPS")
        user.backend = "%s.%s" % ("django.contrib.auth.backends",
                                  "ModelBackend")
        engine = import_module(settings.SESSION_ENGINE)

        # Create a fake request to store login details.
        request = HttpRequest()
        if self.session:
            request.session = self.session
        else:
            request.session = engine.SessionStore()
        login(request, user)

        # Set the cookie to represent the session.
        session_cookie = settings.SESSION_COOKIE_NAME
        self.cookies[session_cookie] = request.session.session_key
        cookie_data = {
            'max-age': None,
            'path': '/',
            'domain': settings.SESSION_COOKIE_DOMAIN,
            'secure': settings.SESSION_COOKIE_SECURE or None,
            'expires': None,
        }
        self.cookies[session_cookie].update(cookie_data)

        # Save the session values.
        request.session.save()

class TestPostDetails(TestCase):
    def setUp(self):
        self.client = TestClient()
        self.credentials = {
            'username': 'testuser',
            'password': 'secret',
            'email': 'test@mail.com',
            'name': 'testuser',
            'instagram': 'https://instagram.com',
            'picture': '',
            'bio': '',
        }
        self.user = User.objects.create_user(**self.credentials)
        self.client.login_user(self.user)
        
        self.location = PostLocation.objects.create(location='testlocation')
        self.category = PostCategory.objects.create(category='testcategory')
        post_data = {
            'user': self.user,
            'name': 'testpost',
            'image': '',
            'price': 1000,
            'description': 'testdescription',
            'instagram_link': '',
        }
        self.post = Post.objects.create(**post_data)
    
    
    # test before signed in
    
    def test_url_add_post_exists_and_redirect_where_not_signed_in(self):
        response = Client().get('/post_details/add')
        self.assertEquals(response.status_code, 302)
    
    def test_url_post_exists_and_not_redirect_where_post_exists_and_not_signed_in(self):
        response = Client().get('/post_details/%d/' % self.post.id)
        self.assertEquals(response.status_code, 200)
        
    def test_url_post_exists_and_redirect_where_post_not_exists_and_not_signed_in(self):
        response = Client().get('/post_details/3/')
        self.assertEquals(response.status_code, 302)
    
    def test_url_delete_post_exists_and_redirect_where_post_exists_and_not_signed_in(self):
        response = Client().post('/post_details/%d/delete' % self.post.id)
        self.assertEquals(response.status_code, 302)
    
    def test_delete_post_exists_and_redirect_where_post_not_exists_and_not_signed_in(self):
        response = Client().post('/post_details/3/delete')
        self.assertEquals(response.status_code, 302)
    
    def test_url_add_review_exists_and_redirect_where_post_exists_and_not_signed_in(self):
        response = Client().get('/post_details/%d/review/add' % self.post.id)
        self.assertEquals(response.status_code, 302)
    
    def test_url_add_review_exists_and_redirect_where_post_not_exists_and_not_signed_in(self):
        response = Client().get('/post_details/3/review/add')
        self.assertEquals(response.status_code, 302)
    
    def test_url_delete_review_exists_and_redirect_where_post_exists_and_not_signed_in(self):
        response = Client().post('/post_details/%d/review/delete' % self.post.id)
        self.assertEquals(response.status_code, 302)
     
    def test_url_delete_review_exists_and_redirect_where_post_not_exists_and_not_signed_in(self):
        response = Client().post('/post_details/3/review/delete')
        self.assertEquals(response.status_code, 302)
        
    
    # test after signed in
    
    def test_url_add_post_exists_where_signed_in(self):
        response = self.client.get('/post_details/add')
        self.assertEquals(response.status_code, 200)
    
    def test_url_delete_post_exists_where_post_exists_and_signed_in(self):
        response = self.client.post('/post_details/%d/delete' % self.post.id)
        self.assertEquals(response.status_code, 200)
    
    def test_url_delete_post_exists_where_post_not_exists_and_signed_in(self):
        response = self.client.post('/post_details/3/delete')
        self.assertEquals(response.status_code, 200)
        
    def test_url_post_exists_where_post_exists_and_signed_in(self):
        response = self.client.get('/post_details/%d/' % self.post.id)
        self.assertEquals(response.status_code, 200)

    def test_url_post_exists_and_redirect_where_post_not_exists_and_signed_in(self):
        response = self.client.get('/post_details/3/')
        self.assertEquals(response.status_code, 302)
    
    def test_url_add_review_exists_where_post_exists_and_signed_in(self):
        response = self.client.get('/post_details/%d/review/add' % self.post.id)
        self.assertEquals(response.status_code, 200)

    def test_url_add_review_exists_and_redirect_where_post_not_exists_and_signed_in(self):
        response = self.client.get('/post_details/3/review/add')
        self.assertEquals(response.status_code, 302)

    def test_url_delete_review_exists_where_post_exists_and_signed_in(self):
        response = self.client.post('/post_details/%d/review/delete' % self.post.id)
        self.assertEquals(response.status_code, 200)

    def test_url_delete_review_exists_where_post_not_exists_and_signed_in(self):
        response = self.client.post('/post_details/3/review/delete')
        self.assertEquals(response.status_code, 200)

    def test_add_post(self):
        data = {
            'user': self.user,
            'name': 'testpost1',
            'image': '',
            'price': 1000,
            'description': 'testdescription1',
            'instagram_link': '',
        }
        response = self.client.post('/post_details/add', data=data)
        self.assertEquals(response.status_code, 200)
        self.assertIn('Post added successfully', response.content.decode('utf-8'))
        
        post = Post.objects.filter(id=2)[0]
        self.assertEquals(post.user, data['user'])
        self.assertEquals(post.name, data['name'])
        self.assertEquals(post.price, data['price'])
        self.assertEquals(post.description, data['description'])
        
    def test_add_review_and_delete_review(self):
        data = {
            'rating': 5,
            'review': 'testreview',
        }
        response = self.client.post('/post_details/%d/review/add' % self.post.id, data=data)
        self.assertEquals(response.status_code, 200)
        self.assertIn('Review added successfully', response.content.decode('utf-8'))
        
        response = self.client.get('/post_details/%d/review/add' % self.post.id)
        self.assertEquals(response.status_code, 302)
        
    def test_post_location(self):
        location = PostLocation.objects.filter(location='testlocation')
        self.assertEquals(location[0], self.location)
        
    def test_post_category(self):
        category = PostCategory.objects.filter(category='testcategory')
        self.assertEquals(category[0], self.category)
    
    def test_reviewAPI_exists(self):
        response = Client().get('/post_details/api/review/get')
        self.assertEquals(response.status_code, 200)
    
    def test_reviewAPI_return_nothing(self):
        response = Client().get('/post_details/api/review/get')
        self.assertIn('No query entered', response.content.decode('utf-8'))
    
    def test_reviewAPI_return_review(self):
        review_data = {
            'user': self.user,
            'post': self.post,
            'rating': 5,
            'review': 'testreview',
        }
        self.post = PostReview.objects.create(**review_data)
        response = Client().get('/post_details/api/review/get?id=1')
        self.assertIn('{"reviews": [{"review_user": "testuser", "review_rating": 5, "review_review": "testreview"}]}', response.content.decode('utf-8'))

    def test_addReviewAPI_success_adding_review(self):
        review_data = {
            'rating': 5,
            'review': 'testreview',
        }
        response = self.client.post('/post_details/api/review/add/%d' % self.post.id, data=review_data)
        self.assertIn('Review added successfully', response.content.decode('utf-8'))
        
    def test_addReviewAPI_failed_adding_review(self):
        review_data = {
            'review': 'testreview',
        }
        response = self.client.post('/post_details/api/review/add/%d' % self.post.id, data=review_data)
        self.assertIn('Failed to add review', response.content.decode('utf-8'))

    def test_deleteReviewAPI_success_delete_review(self):
        review_data = {
            'rating': 5,
            'review': 'testreview',
        }
        self.client.post('/post_details/api/review/add/%d' % self.post.id, data=review_data)
        response = self.client.post('/post_details/api/review/delete/%d' % self.post.id, data=review_data)
        self.assertIn('Review deleted successfully', response.content.decode('utf-8'))
        
    def test_deleteReviewAPI_failed_delete_review(self):
        review_data = {
            'rating': 5,
            'review': 'testreview',
        }
        self.client.post('/post_details/api/review/add/%d' % self.post.id, data=review_data)
        response = self.client.post('/post_details/api/review/delete/%d' % (self.post.id+1), data=review_data)
        self.assertIn('Failed to delete review', response.content.decode('utf-8'))
