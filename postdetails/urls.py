from django.urls import re_path, path
from .views import *

#Website urls
urlpatterns = [
    # re_path(r'^$', index, name = 'index'),
    path('<int:post_id>/', posts, name='posts'),
    path('add', addPost, name='addPost'),
    path('<int:post_id>/delete', deletePost, name='deletePost'),
    path('<int:post_id>/review/add', addReview, name='addReview'),
    path('<int:post_id>/review/delete', deleteReview, name='deleteReview'),
    path('api/review/get', reviewAPI, name='reviewAPI'),
    path('api/review/add/<int:post_id>', addReviewAPI, name='addReviewAPI'),
    path('api/review/delete/<int:post_id>', deleteReviewAPI, name='deleteReviewAPI'),
]
