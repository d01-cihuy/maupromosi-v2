from django import forms
from .models import Post, PostReview, PostLocation, PostCategory

class ReviewPostForm(forms.ModelForm):
    class Meta:
        model = PostReview
        fields = ['rating', 'review']
        exclude = ('user', 'post')

class AddPostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['name', 'image', 'price', 'location', 'category', 'description', 'instagram_link']
        exclude = ('user', )
    location = forms.ModelMultipleChoiceField(queryset=PostLocation.objects.all(), required=False)
    category = forms.ModelMultipleChoiceField(queryset=PostCategory.objects.all(), required=False)
