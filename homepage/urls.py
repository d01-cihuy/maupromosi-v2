from django.urls import re_path, path
from .views import *

# Website urls
urlpatterns = [
    #re_path(r'^$', index, name = 'index'),
    path('', homepage, name='homepage'),
    path('about-us/', about, name='about'),
    path('feedback/', list_feedback, name='feedback'),
    path('feedback/fetch', feedback_fetch, name='feedback_fetch'),
    path('feedback/add', add_feedback, name='add_feedback')
]
