from django.shortcuts import render
from django.http import JsonResponse
from .forms import FormFeedback
from .models import Feedback

# Create your views here.
def index(request):
    return render(request, 'base.html', {})

def homepage(request):
    if request.method == 'POST':
        form = FormFeedback(request.POST)
        email = request.POST.get('email')
        message = request.POST.get('message')
        if form.is_valid() and email != '' and message != '':
            form.save()
            
    form = FormFeedback()
    return render(request, 'homepage/index.html',{'form' : form})

def about(request):
    return render(request, 'homepage/about.html')

def list_feedback(request):
    form = FormFeedback()
    response = {'form':form}
    return render(request, 'homepage/feedback.html', response)

def feedback_fetch(request):
    feedbacks = Feedback.objects.all()
    response = []
    if len(feedbacks) <= 0:
        return JsonResponse({'empty' : 1})
    else:
        for fb in feedbacks:
            tmp = {
                'email' : fb.email,
                'message' : fb.message
            }
            response.append(tmp)
        data = {'feedback' : response}
    return JsonResponse(data)

def add_feedback(request):
    if request.method == 'POST':
        email1 = request.POST.get('email')
        message1 = request.POST.get('message')
        data = {
            'email':email1,
            'message':message1
        }
        Feedback.objects.create(email=email1, message=message1)

    return JsonResponse(data)


    