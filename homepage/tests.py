from django.test import TestCase, Client
from django.urls import resolve
from .models import Feedback
from .views import *
from .forms import FormFeedback

# Create your tests here.
class TestHome(TestCase):
    
    #Test url
    def test_url_homepage(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_url_about(self):
        response = Client().get('/about-us/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_feedback(self):
        response = Client().get('/feedback/')
        self.assertEquals(response.status_code, 200)
    

    #Test templates
    def test_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage/index.html')
    
    def test_using_about_template(self):
        response = Client().get('/about-us/')
        self.assertTemplateUsed(response, 'homepage/about.html')
    
    def test_using_feedback_template(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'homepage/feedback.html')
    

    #Test Views
    def test_homepage_using_correct_views(self):
        view = resolve('/')
        self.assertEqual(view.func, homepage)
    
    def test_about_using_correct_views(self):
        view = resolve('/about-us/')
        self.assertEqual(view.func, about)
    
    def test_feedback_using_correct_views(self):
        view = resolve('/feedback/')
        self.assertEqual(view.func, list_feedback)


    #Test Model
    def test_model_Feedback(self):
        fb = Feedback.objects.create(email='example@mail.com', message='FEEDBACK')
        jumlah= Feedback.objects.all().count()
        self.assertEqual(jumlah,1)
    
    def test_halaman_pakai_index_template_setelah_post(self):
        fb = Feedback.objects.create(email='example@mail.com', message="semangat")
        fb.save()
        response = Client().post('/',{'email':'example@mail.com', 'message':'semangat'})
        self.assertEqual(response.status_code, 200)
    
    #Test form
    def test_form_feedback(self):
        form = FormFeedback()
        form_data = {
            'email':'example@mail.com',
            'message':'INI FEEDBACK'
            }
        form = FormFeedback(data=form_data)
        self.assertTrue(form.is_valid())


    # Test Feedback API
    def test_feedback_fetch_exist(self):
        response = Client().get('/feedback/fetch')
        self.assertEquals(response.status_code, 200)

    def test_add_feedback_works(self):
        response = Client().post('/feedback/add',{'email':'example@mail.com', 'message':'semangat'})
        self.assertEqual(response.status_code, 200)
    
    def test_func_json_fetch(self):
        view = resolve('/feedback/fetch')
        self.assertEquals(view.func, feedback_fetch)
    
    def test_feedback_fetch_empty(self):
        response = Client().get('/feedback/fetch')
        self.assertIn('{"empty": 1}', response.content.decode('utf-8'))
    
    def test_feedback_fetch_display_feedbacks(self):
        fb1 = Feedback.objects.create(email='example@mail.com', message='test1')
        fb2 = Feedback.objects.create(email='test@gmail.com', message='mago should have won soty no cap')
        fb3 = Feedback.objects.create(email='bojong@gede.id', message='namaku elsa lahir di bojonggede')
        fb1.save()
        fb2.save()
        fb3.save()
        response = Client().get('/feedback/fetch')
        self.assertIn('{"email": "example@mail.com", "message": "test1"}', response.content.decode('utf-8'))
        self.assertIn('{"email": "test@gmail.com", "message": "mago should have won soty no cap"}', response.content.decode('utf-8'))
        self.assertIn('{"email": "bojong@gede.id", "message": "namaku elsa lahir di bojonggede"}', response.content.decode('utf-8'))
        

