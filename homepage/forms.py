from django import forms
from .models import Feedback


class FormFeedback(forms.ModelForm):

    email = forms.EmailField(max_length=200, required=False, widget=forms.TextInput(
        attrs={
            'class': 'form-control mx-auto msg',
            'id': 'emailform',
            'placeholder': 'Enter your email address'
        },
    ))
    message = forms.CharField(max_length=200, required=False, widget=forms.TextInput(
        attrs={
            'class': 'form-control mx-auto msg',
            'id': 'textform',
            'placeholder': 'Write your feedback here...'
        },
    ))

    class Meta:
        model = Feedback
        fields = ('email','message')
