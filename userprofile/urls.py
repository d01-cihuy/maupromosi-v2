from django.urls import re_path, path
from .views import *

#Website urls
urlpatterns = [
    #re_path(r'^$', index, name = 'index'),
    path('sign_in', signIn, name='sign_in'),
    path('sign_up', signUp, name='sign_up'),
    path('users/<int:pk>', profile, name='profile'),
    path('sign_out', signOut, name='sign_out'),
    path('edit_profile', editProfile, name='edit_profile'),
    path('edit_password', editPassword, name='edit_password'),
    path('check_username', checkUsername, name = 'check_username'),
]
