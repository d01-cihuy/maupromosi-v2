from postdetails.tests import TestClient
from userprofile.models import User
from postdetails.models import Post

def set_client_and_user(logged_in = True):
    """
    Return client and user for testing
    """
    cresidentials = {
        'username': 'muhamadnickysalim',
        'password': 'apahayo123.', #not a real
        'email': 'muhamadnickysalim@gmail.com',
        'name': 'Muhamad Nicky Salim',
        'instagram': 'https://instagram.com/pewdiepie',
        'picture': '',
        'bio': 'Muehehehehehe',
    }

    client = TestClient()
    user = User.objects.create(**cresidentials)
    if logged_in:
        client.login_user(user)

    return client, user

def generate_data(user):
    names = [
        "Komputer Gaming", "Ceker Ayam Mercon",
        "Jus Jeruk Segar", "Kemeja Alisan",
        "Iphone 8", "Sate Taichan", "Vacuum Cleaner"
    ]
    prices = [20000000, 15000, 8000, 138000, 7300000, 30000, 450000]
    descs = [
        "Komputer terbaru kuat gaming, rendering 3D, dan produktivitas berat",
        "Ceker Ayam Mercon terbaik dengan cabai pilihan, dijamin pedas",
        "Haus? Minum ini bisa buat kamu jauh lebih segar dan bertenaga!",
        "Butuh kemeja untuk kerja atau sehari-hari? Alisan jawabannya!",
        "Ready, iphone 8 baru fullset ori garansi resmi internasional",
        "Sate dengan daging ayam pilihan rendah lemak dan bumbu lezat!",
        "Repot membersihkan debu di tempat sulit? Vacuum Cleaner ini jawabannya!"
    ]

    result = []
    length_data = 7
    for i in range(length_data):
        post_data = {
            'user': user,
            'name': names[i],
            'image': '',
            'price': prices[i],
            'description': descs[i],
            'instagram_link': 'https://instagram.com/pewdiepie',
        }
        result.append(post_data)
    return result

def create_posts_user(user):
    data = generate_data(user)
    posts = []
    for element in data:
        posts.append(Post.objects.create(**element))
    return posts