from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from .models import User
from postdetails.models import Post
from .forms import *

# Create your views here.
def index(request):
    response = {}
    return render(request, 'base.html', response)

def signIn(request):
    if (request.user.is_authenticated):
        return redirect('catalog:katalog')
    response = {}
    if (request.method == 'POST'):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('catalog:katalog')
        else:
            response['message'] = 'Username / Password salah'
    return render(request, 'userprofile/sign_in.html', response)

def signUp(request):
    if (request.user.is_authenticated):
        return redirect('catalog:katalog')
    response = {}
    if (request.method == 'POST'):
        form = SignUpForm(request.POST)
        if (form.is_valid()):
            user = form.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('catalog:katalog')
        else:
            response['form'] = form
    return render(request, 'userprofile/sign_up.html', response)

def signOut(request):
    logout(request)
    return redirect('homepage')

def profile(request, pk):
    response = {} #To do: get user data, user related post(s), make it three per row
    try:
        user = User.objects.filter(id=pk)[0]
        posts = Post.objects.filter(user=user)
        response['posts'] = posts
        response['user'] = user
    except:
        return redirect('catalog:katalog')
    return render(request, 'userprofile/profile.html', response)

def editProfile(request):
    if (not request.user.is_authenticated):
        return redirect('sign_in')
    response = {'form': EditProfile}
    if (request.method == 'POST'):
        form = EditProfile(request.POST, request.FILES, instance=request.user)
        if (form.is_valid()):
            form.save()
            return redirect('profile', pk=request.user.id)
        response['form'] = form
    return render(request, 'userprofile/edit_profile.html', response)

def editPassword(request):
    if (not request.user.is_authenticated):
        return redirect('sign_in')
    response = {'form': EditPassword(request.user)}
    if (request.method == 'POST'):
        form = EditPassword(data=request.POST, user=request.user)
        if (form.is_valid()):
            response['message'] = 'Password berhasil diubah'
            form.save()
        response['form'] = form
    return render(request, 'userprofile/edit_password.html', response)

def checkUsername(request):
    input = request.POST['input']
    result = User.objects.filter(username = input)
    response = {
        'avaiable' : True if len(result) == 0 else False,
    }
    return JsonResponse(response)