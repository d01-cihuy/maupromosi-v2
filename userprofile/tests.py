from userprofile.tests_helper import *
from userprofile.forms import *
from django.test import TestCase, Client
from django.contrib import auth

# Create your tests here.

class Test_sign_up(TestCase):
    def setUp(self):
        self.client, self.user = set_client_and_user(logged_in=True)
        self.template = 'userprofile/sign_up.html'
        self.url = '/user_profile/sign_up'
        self.exist_username = 'muhamadnickysalim'
        self.new_username ='meowmeow'
        self.data = {
            'username' : self.new_username,
            'email' : 'example@gmail.com',
            'name' : 'Meng Garong',
            'instagram' : 'https://instagram.com/pewdiepie',
            'password1' : 'apahayo123.',
            'password2' : 'apahayo123.'
        }

    def test_form(self):
        form = SignUpForm(data=self.data)
        self.assertTrue(form.is_valid())

    def test_sign_up_page_is_exist(self):
        client = Client()
        response= client.get(self.url)
        status_code = response.status_code
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)

    def test_sign_up_while_logged_in(self):
        response = self.client.get(self.url)
        status_code = response.status_code
        user = auth.get_user(self.client)
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)
        self.assertTrue(user.is_authenticated)

    def test_sign_up_valid_data(self):
        data = self.data.copy()
        data['password1'] = 'apahayo123.'
        data['password2'] = 'apahayo123.'
        client = Client()
        response = client.post(self.url, data = data)
        status_code = response.status_code
        user = auth.get_user(client)
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)
        self.assertTrue(user.is_authenticated)

    def test_sign_up_data_invalid(self):
        data = self.data.copy()
        data['username'] = self.exist_username #exist username
        data['email'] = 'hahaha' #invalid email
        data['name'] = '' #blank 
        data['instagram'] = 'maubangetya?'
        data['password1'] = '1' #short and common
        data['password2'] = 'yukbisayuk123' #not same pass
        client = Client()
        response = client.post(self.url, data = data)
        status_code = response.status_code
        user = auth.get_user(client)
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertFalse(user.is_authenticated)
        self.assertIn('error', response.content.decode('utf-8'))
    
class Test_sign_in(TestCase):
    def setUp(self):
        self.client, self.user = set_client_and_user(logged_in=True)
        self.url = '/user_profile/sign_in'
        self.template = 'userprofile/sign_in.html'

    def test_sign_in_page_is_exist(self):
        response = Client().get(self.url)
        status_code = response.status_code
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
    
    def test_sign_in_while_logged_in(self):
        response = self.client.get(self.url)
        status_code = response.status_code
        user = auth.get_user(self.client)
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)
        self.assertTrue(user.is_authenticated)

    def test_sign_in_invalid(self):
        data = {
            'username' : '',
            'password' : ''
        }
        client = Client()
        response = client.post(self.url, data=data)
        status_code = response.status_code
        user = auth.get_user(client)
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertFalse(user.is_authenticated)
        self.assertIn('error', response.content.decode('utf-8'))

class Test_edit_profile(TestCase):
    def setUp(self):
        self.client, self.user = set_client_and_user(logged_in=True)
        self.url = '/user_profile/edit_profile'
        self.template = 'userprofile/edit_profile.html'
        self.data = {
            'email' : 'ugwemuhwem@osas.com',
            'name' : 'uvuvwevwevwe onyetenyevwe',
            'instagram' : 'https://instagram.com/pewdiepie',
            'bio' : 'my name is ...',
            'picture' : ''
        }
    
    def test_form(self):
        form = EditProfile(data = self.data)
        self.assertTrue(form.is_valid())

    def test_edit_profile_page_is_exist(self):
        response = self.client.get(self.url)
        status_code = response.status_code
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
    
    def test_edit_profile_while_logged_out(self):
        client = Client()
        response = client.get(self.url)
        status_code = response.status_code
        user = auth.get_user(client)
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)
        self.assertFalse(user.is_authenticated)

    def test_edit_profile_valid(self):
        response = self.client.post(self.url, data=self.data)
        status_code = response.status_code
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)

    def test_edit_profile_invalid(self):
        data = self.data.copy()
        data['name'] = ''
        response = self.client.post(self.url, data=data)
        status_code = response.status_code
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertIn('error', response.content.decode('utf-8'))


class Test_edit_password(TestCase):
    def setUp(self):
        self.client, self.user = set_client_and_user(logged_in=True)
        self.url = '/user_profile/edit_password'
        self.template = 'userprofile/edit_password.html'

    def test_edit_password_page_is_exist(self):
        response = self.client.get(self.url)
        status_code = response.status_code
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
    
    def test_edit_password_while_logged_out(self):
        client = Client()
        response = client.get(self.url)
        user = auth.get_user(client)
        status_code = response.status_code
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)
        self.assertFalse(user.is_authenticated)

    
    #To do: password valid, password same as before, password invalid (by Ajax)

class Test_sign_out(TestCase):
    def setUp(self):
        self.client, self.user = set_client_and_user(logged_in=True)
        self.url = '/user_profile/sign_out'

    def test_sign_out_logged_in(self):
        response = self.client.get(self.url)
        status_code = response.status_code
        user = auth.get_user(self.client)
        self.assertEqual(status_code, 302)
        self.assertFalse(user.is_authenticated)
    
    def test_sign_out_while_logged_out(self):
        response = Client().get(self.url)
        status_code= response.status_code
        self.assertEqual(status_code, 302)

class Test_profile_info(TestCase):
    def setUp(self):
        self.client, self.user = set_client_and_user(logged_in=True)
        create_posts_user(self.user)
        self.template = 'userprofile/profile.html'
    
    def test_profile_info_while_logged_out_and_doesnt_exist(self):
        response = Client().get('/user_profile/users/0')
        status_code = response.status_code
        self.assertEqual(status_code, 302)
        self.assertTemplateNotUsed(response, self.template)
    
    def test_profile_exist_logged_out(self):
        response = Client().get('/user_profile%s' % self.user.get_absolute_url())
        status_code = response.status_code
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
    
    def test_profile_exist_logged_in(self):
        response = self.client.get('/user_profile%s' % self.user.get_absolute_url())
        status_code = response.status_code
        user = auth.get_user(self.client)
        self.assertEqual(status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertTrue(user.is_authenticated)
    #To do: inspect every detail of profile